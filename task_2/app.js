'use strict';

let express = require('express');
let app = express();
let cors = require('cors');
let fs = require('fs');

app.use(cors());

app.get('/', function (req, res) {
    res.json({
        'hello': 'JS World'
    })
})

app.get('/task2A', function (req, res) {
    let summ = (+req.query.a || 0) + (+req.query.b || 0);

    res.send(summ.toString());
});

app.get('/task2B', function (req, res) {
    let full_name = [],
        surname = '',
        send_name = '',
        query = req.query.fullname.trim().replace(/\s+/g," ");

    fs.appendFile('task2B_log.txt', req.query.fullname+'\n');

    let patt_num = new RegExp('([0-9_/])','gi');
    let num = query.match(patt_num);

    if( num != null ){
        res.send('Invalid fullname');
    }
    

    full_name = query.split(' ');

    if(full_name.length > 3 || req.query.fullname == ''){
        res.send('Invalid fullname');

    } else if(full_name.length === 1) {
        res.send(full_name.toString());
    } else {
        surname = full_name.pop();
        full_name.unshift(surname);

        full_name.forEach(function (elem, index) {
            if(index === 0){
                send_name += elem.charAt(0).toUpperCase()+elem.toLowerCase().slice(1)+' ';
            } else {
                send_name += elem.slice(0,1).toUpperCase()+'. ';
            }

        });

        res.send(send_name.trim());
    }


});

app.get('/task2C', function (req, res) {

    let url = req.query.username;
    const re = new RegExp('@?(https?:)?(\/\/)?((www.)?(telegram|vk|vkontakte)[^\/]*\/)?([a-zA-Z0-9]*)','i')
    const new_user = url.match(re);

    res.send('@'+new_user[6]);
});

app.listen('8080', function () {
    console.log('Example app listening on port 8080');
});